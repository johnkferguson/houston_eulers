# Each new term in the Fibonacci sequence is generated by adding the previous two terms.
# By starting with 1 and 2, the first 10 terms will be:
#
# 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
#
# By considering the terms in the Fibonacci sequence whose values do not exceed four million,
# find the sum of the even-valued terms.

require_relative "../lib/002_fibonacci"

describe Fibonacci do
  it "returns the correct solution" do
    expect(subject.even_sum(4000000)).to eq(4613732)
  end
end