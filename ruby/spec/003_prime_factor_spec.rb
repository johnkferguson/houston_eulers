# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?

require_relative "../lib/003_prime_factor"

describe PrimeFactor do
  describe "#largest_prime_factor_of(number)" do

    context "when the number is a prime number" do
      it "returns the original number" do
        expect(subject.largest_prime_factor_of(7)).to eq(7)
      end
    end

    context "when the square root of the number is a prime number" do
      it "returns the square root" do
        expect(subject.largest_prime_factor_of(49)).to eq(7)
      end
    end

    context "when the number is 13195" do
      it "returns the correct solution" do
        expect(subject.largest_prime_factor_of(13195)).to eq(29)
      end
    end

    context "when the number is 600851475143" do
      it "returns the correct solution" do
        expect(subject.largest_prime_factor_of(600851475143)).to eq(6857)
      end
    end
  end

  describe "#all_primes_less_than(number)" do
    it "returns all of the prime numbers less than a given number" do
      expect(subject.all_primes_less_than(20))
        .to match_array([2, 3, 5, 7, 11, 13, 17, 19])
    end
  end

end
