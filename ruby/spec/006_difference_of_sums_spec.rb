# The sum of the squares of the first ten natural numbers is,
# 1^2 + 2^2 + ... + 10^2 = 385

# The square of the sum of the first ten natural numbers is,
# (1 + 2 + ... + 10)^2 = 55^2 = 3025

# Hence the difference between the sum of the squares of the first ten natural
# numbers and the square of the sum is 3025  385 = 2640.

# Find the difference between the sum of the squares of the first one hundred
# natural numbers and the square of the sum.

require_relative "../lib/006_difference_of_sums"

describe DifferenceOfSums do
  let(:ten_natural) { DifferenceOfSums.new(10) }

  describe "#sum_of_squares" do
    it "returns the correct sum of squares" do
      expect(ten_natural.sum_of_squares).to eq(385)
    end
  end

  describe "#square_of_sums" do
    it "returns the correct square of the sums" do
      expect(ten_natural.square_of_sum).to eq(3025)
    end
  end

  describe "#difference_between" do
    it "provides the correct difference between the sum of square and square of sums" do
      expect(ten_natural.difference_between).to eq(2640)
    end

    context "when trying to solve the problem" do
      it "provides the correct solution" do
        expect(DifferenceOfSums.new(100).difference_between).to eq(25164150)
      end
    end
  end

end