# If we list all the natural numbers below 10 that are multiples of 3 or 5,
# we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

require_relative "../lib/001_multiples"

describe Multiples do
  describe "#sum_multiples_of_three_and_five_less_than(upper_bound)" do
    context "when the upper bound is 10" do
      it "provides the correct soluction" do
        expect(subject.sum_multiples_of_three_and_five_less_than(10)).to eq(23)
      end
    end

    context "when the upper bound is 1000" do
      it "provides the correct solution" do
        expect(subject.sum_multiples_of_three_and_five_less_than(1000))
          .to eq(233168)
      end
    end
  end
end
