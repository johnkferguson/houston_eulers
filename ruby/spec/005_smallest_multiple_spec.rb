# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

require_relative "../lib/005_smallest_multiple"

describe SmallestMultiple do
  describe "#smallest_even_divisible_number" do
    context "when given a range from 1 to 10" do
      it "returns the smallest number divisible by all" do
        expect(subject.smallest_evenly_divisible_number(1, 10)).to eq(2520)
      end
    end

    context "when given a range from 1 to 20" do
      it "returns the smallest number divisible by all" do
        expect(subject.smallest_evenly_divisible_number(1, 20)).to eq("???")
      end
    end

  end
end