class DifferenceOfSums
  attr_reader :upper_bound

  def initialize(upper_bound)
    @upper_bound = upper_bound
  end

  def sum_of_squares
    (1..upper_bound).to_a.inject(0) { |result, element| result + (element * element) }
  end

  def square_of_sum
    sum = (1..upper_bound).to_a.inject(0) { |result, element| result + element }
    sum * sum
  end

  def difference_between
    square_of_sum - sum_of_squares
  end

end
