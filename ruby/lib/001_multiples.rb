class Multiples
  def sum_multiples_of_three_and_five_less_than(upper_bound)
    (1...upper_bound).inject(0) do |result, element|
      (element % 3 == 0 || element % 5 == 0) ? result += element : result
    end
  end
end
