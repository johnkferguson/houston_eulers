class LargeSum
  def first_ten_digits_of_sum(number_array)
    number_array.inject(0) {|sum, num| sum += num }.to_s[0, 10].to_i
  end
end
