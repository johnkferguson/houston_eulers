class Fibonacci
  def even_sum(upper_bound)
    first_num = 0
    second_num = 1
    total_of_even_fib_nums = 0

    (1).upto(upper_bound) do
      if first_num < upper_bound
        sum = first_num + second_num
        first_num = second_num
        second_num = sum
        total_of_even_fib_nums += sum if sum.even? && sum < upper_bound
      end
    end
    total_of_even_fib_nums
  end
end
