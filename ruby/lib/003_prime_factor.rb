require 'prime'

class PrimeFactor

  def largest_prime_factor_of(number)
    return number if Prime.prime?(number)
    sq_root = Math.sqrt(number)
    return sq_root.to_i if sq_root % sq_root.to_i == 0 && Prime.prime?(sq_root)
    all_primes_less_than(sq_root).reverse.find { |prime| number % prime == 0 }
  end

  def all_primes_less_than(number)
    Prime.each(number).inject([]) { |array, prime_num | array << prime_num }
  end

end
